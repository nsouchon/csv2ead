xquery version "3.1";

(:~ This is the default application library module of the csv2ead app.
 : @author Nicolas Souchon
 : @version 1.0.0
 :)

(: Module for app-specific template functions :)
module namespace app = "http://exist-db.org/apps/csv2ead/templates";

import module namespace templates = "http://exist-db.org/xquery/templates";
import module namespace config = "http://exist-db.org/apps/csv2ead/config" at "config.xqm";
import module namespace kwic = "http://exist-db.org/xquery/kwic" at "resource:org/exist/xquery/lib/kwic.xql";
import module namespace util = "http://exist-db.org/xquery/util";

(: For the app:if-attribute-set and app:if-attribute-unset functions, see the tutorial of Loren Cahlander : https://www.youtube.com/watch?v=OlfOCb2-o-Y :)
declare function app:if-attribute-set($node as node(), $model as map(*), $attribute as xs:string) {
    let $isSet := (exists($attribute) and request:get-attribute($attribute))
    return
        if ($isSet)
        then
            templates:process($node/node(), $model)
        else
            ()
};

declare function app:if-attribute-unset($node as node(), $model as map(*), $attribute as xs:string) {
    let $isSet := (exists($attribute) and request:get-attribute($attribute))
    return
        if (not($isSet))
        then
            templates:process($node/node(), $model)
        else
            ()
};

(: This function is similar to the dos2unix command and is used to replace the special characters that cause errors :)
declare function app:dos2unix($doc as xs:string) {
    let $d2u := replace($doc, "(\r)(\n)", "$2")
    let $d2u2 := replace($d2u, "&#160;", " ")
    let $d2u3 := replace($d2u2, "&#65279;", "")
    return
        $d2u3
};

(: The CSV is converted to XML. Each value is placed in an element named after its column header :)
declare function app:csv2xml($csv as xs:string) {
    (: Special characters that cause errors are replaced :)
    let $d2u := app:dos2unix($csv)
    (: We cut the text according to the newlines :)
    let $lines := tokenize($d2u, '\n')
    (: We define the header as the first line :)
    let $head := tokenize($lines[1], ';')
    (: We define the body as everything except the first line :)
    let $body := remove($lines, 1)
    return
        (: We create a <collection> element :)
        element collection {
            for $line in $body
            let $fields := tokenize($line, ';')
            return
                (: For each line we create an <item> element :)
                element item {
                    (: We define the name of the element and its value according to the position of the corresponding token in the line and the header :)
                    for $key at $pos in $head
                    let $value := $fields[$pos]
                    return
                        (: We only create an element if it has a value :)
                        if ($value!="")
                        then
                            element {$key} {$value}
                        else
                            ()
                }
        }
};

(: This function is used to convert a date in ISO 8601 format to text :)
declare function app:convertDate($date as xs:string) {
    (: We cut the date according to the dashes :)
    let $tokenizeDate := tokenize($date, "-")
    (: The first token corresponds to the year :)
    let $year := $tokenizeDate[1]
    (: The second token corresponds to the month to convert to text :)
    let $month := switch ($tokenizeDate[2])
                  case "01" return "janvier"
                  case "02" return "février"
                  case "03" return "mars"
                  case "04" return "avril"
                  case "05" return "mai"
                  case "06" return "juin"
                  case "07" return "juillet"
                  case "08" return "août"
                  case "09" return "septembre"
                  case "10" return "octobre"
                  case "11" return "novembre"
                  case "12" return "décembre"
                  default return ""
    (: The third segment corresponds to the days :)
    let $day := $tokenizeDate[3]
    return
        (: We generate the date according to the number of tokens :)
        if ($day)
        then
            string-join(($day, $month, $year), " ")
        else
            if ($month)
            then
                string-join(($month, $year), " ")
            else
                $year
};

(: This function is used to generate the <unitdate> element :)
declare function app:unitDate($dates as xs:string) {
    for $date in tokenize($dates, "\|")
    return
        <unitdate era="ce" calendar="gregorian" normal="{$dates}">{
            (: If the date corresponds to an interval, :)
            if (contains($date, "/"))
            then
                (: we convert each part of the interval into text :)
                let $d := for $a in tokenize($date, "/")
                          let $v := app:convertDate($a)
                          return
                              $v
                return
                    (: and we format it :)
                    string-join($d, "-")
            else
                (: Otherwise, we convert the date :)
                app:convertDate($date)
        }</unitdate>
};

(: This function is used to generate the <publicationStmt> element based on the selected institution :)
declare function app:publicationStmt($efe as xs:string) {
    (: We retrieve the current date :)
    let $a := fn:current-date()
    (: We only keep the date, month and day of the current date :)
    let $date := substring-before($a, "+")
    return
        (: We generate the element according to the institution and the current date :)
        switch ($efe)
        case "IFAO" return
            <publicationstmt>
                <publisher encodinganalog="publisher">Institut français d'archéologie orientale</publisher>
                <address>
                    <addressline>37 rue al-Cheikh Aly Youssef</addressline>
                    <addressline>B.P. Qasr al-Ainy 11562</addressline>
                    <addressline>11441</addressline>
                    <addressline>Le Caire</addressline>
                    <addressline>Égypte</addressline>
                    <addressline>Téléphone : (+20) 2 279 00 255</addressline>
                    <addressline>Courriel : archives@ifao.egnet.net</addressline>
                    <addressline>https://www.ifao.egnet.net/archives-scientifiques/archives-presentation/</addressline>
                </address>
                <date normal="{$date}" encodinganalog="date">{$date}</date>
            </publicationstmt>
        case "EFA" return
            <publicationstmt>
                <publisher encodinganalog="publisher">École française d’Athènes</publisher>
                <address>
                    <addressline>6 rue Didotou</addressline>
                    <addressline>10680</addressline>
                    <addressline>Athènes</addressline>
                    <addressline>Grèce</addressline>
                    <addressline>Téléphone : (+30) 210 36 79 942</addressline>
                    <addressline>Courriel : archives@efa.gr</addressline>
                    <addressline>https://www.efa.gr/presentation-generale-des-archives/</addressline>
                </address>
                <date normal="{$date}" encodinganalog="date">{$date}</date>
            </publicationstmt>
        case "EFR" return
            <publicationstmt>
                <publisher encodinganalog="publisher">École française de Rome</publisher>
                <address>
                    <addressline>62 place Navone</addressline>
                    <addressline>00186</addressline>
                    <addressline>Rome</addressline>
                    <addressline>Italie</addressline>
                    <addressline>Téléphone : (+39) 06 68 42 95 06</addressline>
                    <addressline>Courriel : emmanuel.turquin@efrome.it</addressline>
                    <addressline>https://www.efrome.it/la-recherche/archives</addressline>
                </address>
                <date normal="{$date}" encodinganalog="date">{$date}</date>
            </publicationstmt>
        case "EFEO" return
            <publicationstmt>
                <publisher encodinganalog="publisher">Archives de l'École française d’Extrême-Orient (Paris)</publisher>
                <address>
                    <addressline>22 avenue du Président Wilson</addressline>
                    <addressline>75116</addressline>
                    <addressline>Paris</addressline>
                    <addressline>France</addressline>
                    <addressline>Téléphone : (+33) 01 53 70 18 46</addressline>
                    <addressline>Fax : (+33) 01 53 70 87 60</addressline>
                    <addressline>Courriel : bibliotheque@efeo.net</addressline>
                </address>
                <date normal="{$date}" encodinganalog="date">{$date}</date>
            </publicationstmt>
        case "CVZ" return
            <publicationstmt>
                <publisher encodinganalog="publisher">Casa de Velázquez</publisher>
                <address>
                    <addressline>Casa de Velázquez</addressline>
                    <addressline>Calle Paul Guinard 3</addressline>
                    <addressline>28040</addressline>
                    <addressline>Madrid</addressline>
                    <adressline>Espagne</adressline>
                    <addressline>Téléphone : (+34) 914 551 580</addressline>
                    <addressline>Courriel : bcv@casadevelazquez.org</addressline>
                    <adressline>https://www.casadevelazquez.org/bibliotheque/les-archives-de-la-casa-de-velazquez</adressline>
                </address>
                <date normal="{$date}" encodinganalog="date">{$date}</date>
            </publicationstmt>
        default return ""
};

(: This function is used to convert ISO 639-2 language codes to text :)
declare function app:langcode($lang as xs:string) {
    switch ($lang)
    case "abk" return "abkhaze"
    case "ace" return "aceh"
    case "ach" return "acoli"
    case "ada" return "adangme"
    case "ady" return "adyghé"
    case "aar" return "afar"
    case "afh" return "afrihili"
    case "afr" return "afrikaans"
    case "afa" return "afro-asiatiques, langues"
    case "ain" return "aïnou"
    case "aka" return "akan"
    case "akk" return "akkadien"
    case "alb" return "albanais"
    case "sqi" return "albanais"
    case "ale" return "aléoute"
    case "alg" return "algonquines, langues"
    case "ger" return "allemand"
    case "deu" return "allemand"
    case "gmh" return "allemand, moyen haut (ca. 1050-1500)"
    case "goh" return "allemand, vieux haut (ca. 750-1050)"
    case "alt" return "altai du Sud"
    case "tut" return "altaïques, langues"
    case "zgh" return "amazighe standard marocain"
    case "cai" return "amérindiennes de l'Amérique centrale, langues"
    case "amh" return "amharique"
    case "anp" return "angika"
    case "eng" return "anglais"
    case "enm" return "anglais moyen (1100-1500)"
    case "ang" return "anglo-saxon (ca.450-1100)"
    case "apa" return "apaches, langues"
    case "ara" return "arabe"
    case "arg" return "aragonais"
    case "arc" return "araméen d'empire (700-300 BCE)"
    case "arp" return "arapaho"
    case "arw" return "arawak"
    case "arm" return "arménien"
    case "hye" return "arménien"
    case "rup" return "aroumain; macédo-roumain"
    case "art" return "artificielles, langues"
    case "asm" return "assamais"
    case "ast" return "asturien; bable; léonais; asturoléonais"
    case "ath" return "athapascanes, langues"
    case "aus" return "australiennes, langues"
    case "map" return "austronésiennes, langues"
    case "ava" return "avar"
    case "ave" return "avestique"
    case "awa" return "awadhi"
    case "aym" return "aymara"
    case "aze" return "azéri"
    case "bak" return "bachkir"
    case "ban" return "balinais"
    case "bal" return "baloutchi"
    case "bat" return "baltes, langues"
    case "bam" return "bambara"
    case "bai" return "bamiléké, langues"
    case "bad" return "banda, langues"
    case "bnt" return "bantou, langues"
    case "nds" return "bas allemand; bas saxon; allemand, bas; saxon, bas"
    case "bas" return "basa"
    case "baq" return "basque"
    case "eus" return "basque"
    case "dsb" return "bas-sorabe"
    case "btk" return "batak, langues"
    case "bej" return "bedja"
    case "bem" return "bemba"
    case "ben" return "bengali"
    case "ber" return "berbères, langues"
    case "bho" return "bhojpuri"
    case "bis" return "bichlamar"
    case "bel" return "biélorusse"
    case "bik" return "bikol"
    case "bin" return "bini; edo"
    case "bur" return "birman"
    case "mya" return "birman"
    case "bla" return "blackfoot"
    case "byn" return "blin; bilen"
    case "bos" return "bosniaque"
    case "bua" return "bouriate"
    case "bra" return "braj"
    case "bre" return "breton"
    case "bug" return "bugi"
    case "bul" return "bulgare"
    case "cad" return "caddo"
    case "krl" return "carélien"
    case "cat" return "catalan; valencien"
    case "cau" return "caucasiennes, langues"
    case "ceb" return "cebuano"
    case "cel" return "celtiques, langues; celtes, langues"
    case "cmc" return "chames, langues"
    case "cha" return "chamorro"
    case "shn" return "chan"
    case "chr" return "cherokee"
    case "chy" return "cheyenne"
    case "chb" return "chibcha"
    case "nya" return "chichewa; chewa; nyanja"
    case "chi" return "chinois"
    case "zho" return "chinois"
    case "chn" return "chinook, jargon"
    case "chp" return "chipewyan"
    case "cho" return "choctaw"
    case "chk" return "chuuk"
    case "cop" return "copte"
    case "kor" return "coréen"
    case "cor" return "cornique"
    case "cos" return "corse"
    case "cus" return "couchitiques, langues"
    case "cre" return "cree"
    case "crp" return "créoles et pidgins"
    case "cpe" return "créoles et pidgins basés sur l'anglais"
    case "cpf" return "créoles et pidgins basés sur le français"
    case "cpp" return "créoles et pidgins basés sur le portugais"
    case "hrv" return "croate"
    case "dak" return "dakota"
    case "dan" return "danois"
    case "dar" return "dargwa"
    case "day" return "dayak, langues"
    case "del" return "delaware"
    case "din" return "dinka"
    case "dyu" return "dioula"
    case "chg" return "djaghataï"
    case "doi" return "dogri"
    case "dgr" return "dogrib"
    case "dua" return "douala"
    case "dra" return "dravidiennes, langues"
    case "dzo" return "dzongkha"
    case "sco" return "écossais"
    case "efi" return "efik"
    case "egy" return "égyptien"
    case "eka" return "ekajuk"
    case "elx" return "élamite"
    case "myv" return "erza"
    case "den" return "esclave (athapascan)"
    case "spa" return "espagnol; castillan"
    case "epo" return "espéranto"
    case "est" return "estonien"
    case "ewe" return "éwé"
    case "ewo" return "éwondo"
    case "fan" return "fang"
    case "fat" return "fanti"
    case "fao" return "féroïen"
    case "fij" return "fidjien"
    case "fil" return "filipino; pilipino"
    case "fin" return "finnois"
    case "fiu" return "finno-ougriennes, langues"
    case "fon" return "fon"
    case "fre" return "français"
    case "fra" return "français"
    case "fro" return "français ancien (842-ca.1400)"
    case "frm" return "français moyen (1400-1600)"
    case "fur" return "frioulan"
    case "fry" return "frison occidental"
    case "frs" return "frison oriental"
    case "frr" return "frison septentrional"
    case "gaa" return "ga"
    case "gla" return "gaélique; gaélique écossais"
    case "glg" return "galicien"
    case "orm" return "galla"
    case "wel" return "gallois"
    case "cym" return "gallois"
    case "lug" return "ganda"
    case "gay" return "gayo"
    case "gba" return "gbaya"
    case "geo" return "géorgien"
    case "kat" return "géorgien"
    case "gem" return "germaniques, langues"
    case "gon" return "gond"
    case "gor" return "gorontalo"
    case "got" return "gothique"
    case "guj" return "goudjrati"
    case "grb" return "grebo"
    case "grc" return "grec ancien (jusqu'à 1453)"
    case "gre" return "grec moderne (après 1453)"
    case "ell" return "grec moderne (après 1453)"
    case "kal" return "groenlandais"
    case "grn" return "guarani"
    case "gez" return "guèze"
    case "gwi" return "gwich'in"
    case "hai" return "haida"
    case "hat" return "haïtien; créole haïtien"
    case "hau" return "haoussa"
    case "hsb" return "haut-sorabe"
    case "haw" return "hawaïen"
    case "heb" return "hébreu"
    case "her" return "herero"
    case "hil" return "hiligaynon"
    case "hin" return "hindi"
    case "hmo" return "hiri motu"
    case "hit" return "hittite"
    case "hmn" return "hmong"
    case "hun" return "hongrois"
    case "hup" return "hupa"
    case "sah" return "iakoute"
    case "iba" return "iban"
    case "ido" return "ido"
    case "ibo" return "igbo"
    case "ijo" return "ijo, langues"
    case "ilo" return "ilocano"
    case "und" return "indéterminée"
    case "inc" return "indo-aryennes, langues"
    case "ine" return "indo-européennes, langues"
    case "ind" return "indonésien"
    case "inh" return "ingouche"
    case "ina" return "interlingua (langue auxiliaire internationale)"
    case "ile" return "interlingue"
    case "iku" return "inuktitut"
    case "ipk" return "inupiaq"
    case "ira" return "iraniennes, langues"
    case "gle" return "irlandais"
    case "sga" return "irlandais ancien (jusqu'à 900)"
    case "mga" return "irlandais moyen (900-1200)"
    case "iro" return "iroquoises, langues"
    case "ice" return "islandais"
    case "isl" return "islandais"
    case "ita" return "italien"
    case "jpn" return "japonais"
    case "jav" return "javanais"
    case "jrb" return "judéo-arabe"
    case "lad" return "judéo-espagnol"
    case "jpr" return "judéo-persan"
    case "kbd" return "kabardien"
    case "kab" return "kabyle"
    case "kac" return "kachin; jingpho"
    case "csb" return "kachoube"
    case "xal" return "kalmouk; oïrat"
    case "kam" return "kamba"
    case "kan" return "kannada"
    case "kau" return "kanouri"
    case "kaa" return "karakalpak"
    case "krc" return "karatchai balkar"
    case "kar" return "karen, langues"
    case "car" return "karib; galibi; carib"
    case "kas" return "kashmiri"
    case "kaw" return "kawi"
    case "kaz" return "kazakh"
    case "kha" return "khasi"
    case "khm" return "khmer central"
    case "khi" return "khoïsan, langues"
    case "kho" return "khotanais; sakan"
    case "kik" return "kikuyu"
    case "kmb" return "kimbundu"
    case "kir" return "kirghiz"
    case "gil" return "kiribati"
    case "tlh" return "klingon"
    case "kom" return "kom"
    case "kon" return "kongo"
    case "kok" return "konkani"
    case "kos" return "kosrae"
    case "kum" return "koumyk"
    case "kpe" return "kpellé"
    case "kro" return "krou, langues"
    case "kua" return "kuanyama; kwanyama"
    case "kur" return "kurde"
    case "kru" return "kurukh"
    case "kut" return "kutenai"
    case "lah" return "lahnda"
    case "lam" return "lamba"
    case "bih" return "langues biharis"
    case "sgn" return "langues des signes"
    case "him" return "langues himachalis; langues paharis occidentales"
    case "mis" return "langues non codées"
    case "lao" return "lao"
    case "lat" return "latin"
    case "lav" return "letton"
    case "lez" return "lezghien"
    case "lim" return "limbourgeois"
    case "lin" return "lingala"
    case "lit" return "lituanien"
    case "jbo" return "lojban"
    case "loz" return "lozi"
    case "lub" return "luba-katanga"
    case "lua" return "luba-lulua"
    case "lui" return "luiseno"
    case "lun" return "lunda"
    case "luo" return "luo (Kenya et Tanzanie)"
    case "lus" return "lushai"
    case "ltz" return "luxembourgeois"
    case "mac" return "macédonien"
    case "mkd" return "macédonien"
    case "mad" return "madourais"
    case "mag" return "magahi"
    case "mai" return "maithili"
    case "mak" return "makassar"
    case "may" return "malais"
    case "msa" return "malais"
    case "mal" return "malayalam"
    case "div" return "maldivien"
    case "mlg" return "malgache"
    case "mlt" return "maltais"
    case "mdr" return "mandar"
    case "mnc" return "mandchou"
    case "man" return "mandingue"
    case "mni" return "manipuri"
    case "mno" return "manobo, langues"
    case "glv" return "manx; mannois"
    case "mao" return "maori"
    case "mri" return "maori"
    case "arn" return "mapudungun; mapuche; mapuce"
    case "mar" return "marathe"
    case "chm" return "mari"
    case "mah" return "marshall"
    case "mwr" return "marvari"
    case "mas" return "massaï"
    case "myn" return "maya, langues"
    case "men" return "mendé"
    case "mic" return "mi'kmaq; micmac"
    case "min" return "minangkabau"
    case "mwl" return "mirandais"
    case "moh" return "mohawk"
    case "mdf" return "moksa"
    case "lol" return "mongo"
    case "mon" return "mongol"
    case "mkh" return "môn-khmer, langues"
    case "cnr" return "monténégrin"
    case "mos" return "moré"
    case "mun" return "mounda, langues"
    case "mul" return "multilingue"
    case "mus" return "muskogee"
    case "nah" return "nahuatl, langues"
    case "nap" return "napolitain"
    case "nau" return "nauruan"
    case "nav" return "navaho"
    case "nde" return "ndébélé du Nord"
    case "nbl" return "ndébélé du Sud"
    case "ndo" return "ndonga"
    case "dum" return "néerlandais moyen (ca. 1050-1350)"
    case "dut" return "néerlandais; flamand"
    case "nld" return "néerlandais; flamand"
    case "new" return "nepal bhasa; newari"
    case "nep" return "népalais"
    case "nwc" return "newari classique"
    case "nia" return "nias"
    case "nic" return "nigéro-kordofaniennes, langues"
    case "ssa" return "nilo-sahariennes, langues"
    case "niu" return "niué"
    case "nqo" return "n'ko"
    case "nog" return "nogaï; nogay"
    case "nai" return "nord-amérindiennes, langues"
    case "non" return "norrois, vieux"
    case "nor" return "norvégien"
    case "nob" return "norvégien bokmål"
    case "nno" return "norvégien nynorsk; nynorsk, norvégien"
    case "nub" return "nubiennes, langues"
    case "nym" return "nyamwezi"
    case "nyn" return "nyankolé"
    case "nyo" return "nyoro"
    case "nzi" return "nzema"
    case "oci" return "occitan (après 1500)"
    case "oji" return "ojibwa"
    case "ori" return "oriya"
    case "osa" return "osage"
    case "oss" return "ossète"
    case "oto" return "otomi, langues"
    case "udm" return "oudmourte"
    case "uga" return "ougaritique"
    case "uig" return "ouïgour"
    case "urd" return "ourdou"
    case "uzb" return "ouszbek"
    case "pus" return "pachto"
    case "pal" return "pahlavi"
    case "pau" return "palau"
    case "pli" return "pali"
    case "pam" return "pampangan"
    case "pag" return "pangasinan"
    case "pap" return "papiamento"
    case "paa" return "papoues, langues"
    case "zxx" return "pas de contenu linguistique; non applicable"
    case "nso" return "pedi; sepedi; sotho du Nord"
    case "pan" return "pendjabi"
    case "per" return "persan"
    case "fas" return "persan"
    case "peo" return "perse, vieux (ca. 600-400 av. J.-C.)"
    case "ful" return "peul"
    case "phn" return "phénicien"
    case "phi" return "philippines, langues"
    case "pon" return "pohnpei"
    case "pol" return "polonais"
    case "por" return "portugais"
    case "pra" return "prâkrit, langues"
    case "pro" return "provençal ancien (jusqu'à 1500); occitan ancien (jusqu'à 1500)"
    case "que" return "quechua"
    case "raj" return "rajasthani"
    case "rap" return "rapanui"
    case "rar" return "rarotonga; maori des îles Cook"
    case "qaa-qtz" return "réservée à l'usage local"
    case "roh" return "romanche"
    case "roa" return "romanes, langues"
    case "rum" return "roumain; moldave"
    case "ron" return "roumain; moldave"
    case "run" return "rundi"
    case "rus" return "russe"
    case "kin" return "rwanda"
    case "sal" return "salishennes, langues"
    case "sam" return "samaritain"
    case "smi" return "sames, langues"
    case "smj" return "sami de Lule"
    case "smn" return "sami d'Inari"
    case "sme" return "sami du Nord"
    case "sma" return "sami du Sud"
    case "sms" return "sami skolt"
    case "smo" return "samoan"
    case "sad" return "sandawe"
    case "sag" return "sango"
    case "san" return "sanskrit"
    case "sat" return "santal"
    case "srd" return "sarde"
    case "sas" return "sasak"
    case "sel" return "selkoupe"
    case "sem" return "sémitiques, langues"
    case "srp" return "serbe"
    case "srr" return "sérère"
    case "sna" return "shona"
    case "scn" return "sicilien"
    case "sid" return "sidamo"
    case "snd" return "sindhi"
    case "sin" return "singhalais"
    case "sit" return "sino-tibétaines, langues"
    case "sio" return "sioux, langues"
    case "sla" return "slaves, langues"
    case "chu" return "slavon d'église; vieux slave; slavon liturgique; vieux bulgare"
    case "slo" return "slovaque"
    case "slk" return "slovaque"
    case "slv" return "slovène"
    case "sog" return "sogdien"
    case "som" return "somali"
    case "son" return "songhai, langues"
    case "snk" return "soninké"
    case "wen" return "sorabes, langues"
    case "sot" return "sotho du Sud"
    case "sun" return "soundanais"
    case "sus" return "soussou"
    case "srn" return "sranan tongo"
    case "sai" return "sud-amérindiennes, langues"
    case "swe" return "suédois"
    case "gsw" return "suisse alémanique; alémanique; alsacien"
    case "suk" return "sukuma"
    case "sux" return "sumérien"
    case "swa" return "swahili"
    case "ssw" return "swati"
    case "zbl" return "symboles Bliss; Bliss"
    case "syr" return "syriaque"
    case "syc" return "syriaque classique"
    case "tgk" return "tadjik"
    case "tgl" return "tagalog"
    case "tah" return "tahitien"
    case "tai" return "tai, langues"
    case "tmh" return "tamacheq"
    case "tam" return "tamoul"
    case "tat" return "tatar"
    case "crh" return "tatar de Crimé"
    case "cze" return "tchèque"
    case "ces" return "tchèque"
    case "che" return "tchétchène"
    case "chv" return "tchouvache"
    case "tel" return "télougou"
    case "tem" return "temne"
    case "ter" return "tereno"
    case "tet" return "tetum"
    case "tha" return "thaï"
    case "tib" return "tibétain"
    case "bod" return "tibétain"
    case "tig" return "tigré"
    case "tir" return "tigrigna"
    case "tiv" return "tiv"
    case "tli" return "tlingit"
    case "tpi" return "tok pisin"
    case "tkl" return "tokelau"
    case "tog" return "tonga (Nyasa)"
    case "ton" return "tongan (Îles Tonga)"
    case "tyv" return "touva"
    case "rom" return "tsigane"
    case "tsi" return "tsimshian"
    case "tso" return "tsonga"
    case "tsn" return "tswana"
    case "tum" return "tumbuka"
    case "tup" return "tupi, langues"
    case "tur" return "turc"
    case "ota" return "turc ottoman (1500-1928)"
    case "tuk" return "turkmène"
    case "tvl" return "tuvalu"
    case "twi" return "twi"
    case "ukr" return "ukrainien"
    case "umb" return "umbundu"
    case "vai" return "vaï"
    case "ven" return "venda"
    case "vie" return "vietnamien"
    case "vol" return "volapük"
    case "vot" return "vote"
    case "wak" return "wakashanes, langues"
    case "wln" return "wallon"
    case "war" return "waray"
    case "was" return "washo"
    case "wal" return "wolaitta; wolaytta"
    case "wol" return "wolof"
    case "xho" return "xhosa"
    case "yao" return "yao"
    case "yap" return "yapois"
    case "iii" return "yi de Sichuan"
    case "yid" return "yiddish"
    case "yor" return "yoruba"
    case "ypk" return "yupik, langues"
    case "znd" return "zandé, langues"
    case "zap" return "zapotèque"
    case "zza" return "zaza; dimili; dimli; kirdki; kirmanjki; zazaki"
    case "zen" return "zenaga"
    case "zha" return "zhuang; chuang"
    case "zul" return "zoulou"
    case "zun" return "zuni"
    default return ""
};

(: This function is used to generate the <langmaterial> element :)
declare function app:langmaterial($c as xs:string) {
    <langmaterial encodinganalog="3.4.3">{
        (: We create a <language> element per attested language :)
        for $lang in tokenize($c, "\|")
        return
            if (contains($lang, "-"))
            then
                let $l := tokenize($lang, "-")[1]
                let $s := tokenize($lang, "-")[2]
                return
                    (: If the language is associated with a writing system, indicate the ISO 15924 code in the @scriptcode attribute of the <language> element :)
                    <language langcode="{$l}" scriptcode="{$s}">{app:langcode($l)}</language>
            else
                <language langcode="{$lang}">{app:langcode($lang)}</language>
    }</langmaterial>
};

(: This function is used to define the country code according to the selected institution :)
declare function app:countryCode($efe as xs:string) {
    switch ($efe)
    case "IFAO" return "EG"
    case "EFA" return "GR"
    case "EFR" return "IT"
    case "EFEO" return "FR"
    case "CVZ" return "ES"
    default return ""
};

(: This function is used to generate the <eadheader> element :)
declare function app:eadHeader($doc as node(), $efe as xs:string) {
    let $e1 := $doc/child::item[child::niveauDescription="Fonds" or child::niveauDescription="Collection"]
    let $title := $e1/child::titre
    (: Replace spaces in the title with hyphens :)
    let $fTitle := translate(lower-case($title), " ", "-")
    let $id := $e1/child::cote
    let $lang := $e1/child::langue
    return
        <eadheader langencoding="iso639-2b" countryencoding="iso3166-1" dateencoding="iso8601" repositoryencoding="iso15511" scriptencoding="iso15924" relatedencoding="DC">
            <eadid identifier="{$fTitle}" countrycode="{app:countryCode($efe)}" mainagencycode="{$efe}" encodinganalog="identifier">{$id/string()}</eadid>
            <filedesc>
                <titlestmt>
                    <titleproper encodinganalog="title">{$title/string()}</titleproper>
                </titlestmt>
                {
                    (: We use the app:publicationStmt function to generate the <publicationSmt> element depending on the selected institution :)
                    app:publicationStmt($efe)
                }
            </filedesc>
            <profiledesc>
                <creation>Créé selon la DTD EAD 2002 en utilisant l'application csv2ead.</creation>
                <langusage>
                    <language langcode="fre">Français</language>
                </langusage>
            </profiledesc>
        </eadheader>
};

(: This function is used to generate the accession number of the element :)
declare function app:cote($doc as node(), $item as node(), $efe as xs:string) {
    let $cote := $item/child::cote
    let $code := app:countryCode($efe)
    let $parentId := $item/child::identifiantParent
    let $parent := $doc/child::item[child::identifiant=$parentId]
    let $coteParent := $parent/child::cote
    return
        if ($parent)
        then
            (: If this element has a parent, we add the parent's accession number before the element's accession number :)
            (: This function is recursive, so the accession number of the element contains that of its parent but also of its ancestors :)
            string-join((app:cote($doc, $parent, $efe), $cote), "-")
        else
            string-join(($code, $efe, $cote), "-")
};

(: This function is used to generate the <did> element :)
declare function app:did($doc as node(), $item as node(), $efe as xs:string) {
    let $title := $item/child::titre
    let $cote := app:cote($doc, $item, $efe)
    let $code := app:countryCode($efe)
    let $date := $item/child::date
    let $physdesc := $item/child::importanceMaterielle
    let $lang := $item/child::langue
    let $repository := $item/child::lieuConservation
    let $sources := $item/child::sourcesUtilisees
    let $producteurPerson := $item/child::producteurPersonne
    let $producteurOrganism := $item/child::producteurOrganisme
    return
        <did>
            <unitid countrycode="{$code}" repositorycode="{$efe}" encodinganalog="3.1.1">{$cote}</unitid>
            <unittitle encodinganalog="3.1.2">{$title/string()}</unittitle>
            {app:unitDate($date)}
            <physdesc encodinganalog="3.1.5">
                <extent>{$physdesc/string()}</extent>
            </physdesc>
            <repository>
                <corpname>{$repository/string()}</corpname>
            </repository>
            {
                if ($lang)
                then
                    app:langmaterial($lang)
                else
                    ()
            }
            {
                if ($sources)
                then
                    <note type="sourcesDescription">
                        <list>{
                            for $source in tokenize($sources, "\|")
                            return
                                <item>{$source}</item>
                        }</list>
                    </note>
                else
                    ()
            }
            <origination encodinganalog="3.2.1">{
                let $corpname := for $organism in tokenize($producteurOrganism, "\|")
                                 return
                                    <corpname normal="{$organism}" role="producteur">{$organism}</corpname>
                let $persname := for $person in tokenize($producteurPerson, "\|")
                                 return
                                    <persname normal="{$person}" role="producteur">{$person}</persname>
                return
                    ($corpname, $persname)
            }</origination>
        </did>
};

(: This function is used to generate the <c> element :)
declare function app:c($doc as node(), $item as node(), $efe as xs:string) {
    let $id := $item/child::identifiant
    let $level := app:translateLevel($item/child::niveauDescription)
    return
        <c level="{$level}">
            {
                (: We generate the <did> element :)
                app:did($doc, $item, $efe)
            }
            {
                (: We generate all the other non-child elements of <did> :)
                app:description($item, $efe)
            }
            {
                (: If <item>s are parents of a <identifiantParent> element whose value is equal to the identifier of the current element, we generate the <c> elements of these <item>s :)
                if ($doc/child::item[child::identifiantParent=$id])
                then
                    for $item in $doc/child::item[child::identifiantParent=$id]
                    return
                        app:c($doc, $item, $efe)
                else
                    ()
            }
        </c>
};

(: This function is used to generate the <arrangement> element :)
declare function app:arrangement($item as node(), $efe as xs:string) {
    let $collection := $item/parent::node()
    let $level := $item/child::niveauDescription
    return
        (: If the current element is not a parent of a <niveauDescription> element with the value "Dossier" or "Pièce" :)
        if ($level!="Dossier" and $level!="Pièce")
        then
            <arrangement encodinganalog="3.3.4">
                <list>{
                    (: We retrieve the accession number and title of each child element of the current element :)
                    let $id := $item/child::identifiant
                    for $childs in $item/following-sibling::item[child::identifiantParent=$id]
                    let $title := $childs/child::titre
                    return
                        <item>{app:cote($collection, $childs, $efe) || " : " || $title}</item>
                }</list>
            </arrangement>
        else
            ()
};

(: This function is used to generate the <accessrestrict> element based on the selected institution :)
declare function app:accessrestrict($efe as xs:string) {
    <accessrestrict encodinganalog="3.4.1">
        <p>{
            switch ($efe)
            case "IFAO" return "Les archives dites « manuscrites » de l’Ifao sont consultables sur autorisation préalable du responsable des archives et sur rendez-vous uniquement (archives@ifao.egnet.net), du dimanche au jeudi de 8h à 13h et de 14h à 16h40."
            case "EFA" return "La consultation des archives conservées par l’École française d’Athènes se fait dans le respect des dispositions prévues par la législation en vigueur, en particulier les codes du patrimoine, de la propriété intellectuelle, des relations entre le public et l’administration, et le code de la recherche."
            case "EFR" return "Les archives sont consultables sur autorisation préalable du responsable des archives et sur rendez-vous uniquement."
            case "EFEO" return "Accès à toute personne majeure, sous réserve de la justification d'une recherche. L'inscription à la bibliothèque doit être effectuée en amont de la consultation."
            case "CVZ" return "La consultation, la reproduction et la réutilisation des documents d’archives se font dans le respect des dispositions prévues par la législation en vigueur, en particulier les codes du patrimoine, de la propriété intellectuelle, des relations entre le public et l’administration. Certains documents pouvant contenir des informations couvertes par le respect de la vie privée (délai de communicabilité de 50 ans) ou contenant des données à caractère personnel ne sont pas communicables."
            default return ""
        }</p>
    </accessrestrict>
};

(: This function is used to generate the <userestrict> element based on the selected institution :)
declare function app:userestrict($efe as xs:string) {
    <userestrict encodinganalog="3.4.2">
        <p>{
            switch ($efe)
            case "IFAO" return "La reproduction et la réutilisation de ces dossiers se font dans le respect des dispositions prévues par le code français de la propriété intellectuelle (art. L.123-1 à 4). Les références exactes et complètes des auteurs et des documents (cote, description, dates), ainsi que la mention « Archives de l’Ifao », sont obligatoires en légende des reproductions diffusées."
            case "EFA" return "La reproduction et la réutilisation des archives conservées par l’École française d’Athènes se fait dans le respect des dispositions prévues par la législation en vigueur, en particulier les codes du patrimoine, de la propriété intellectuelle, des relations entre le public et l’administration, et le code de la recherche."
            case "CVZ" return "Documents sous licence Creative Commons Attribution-NonCommercial-ShareAlike 4.0."
            default return ""
        }</p>
    </userestrict>
};

(: This function is used to generate non-child elements of <did> based on their names and attribute values :)
declare function app:element($item as node(), $cell as xs:string, $element as xs:string, $attributeValue as xs:string?) {
    let $ex := $item/child::*[local-name()=$cell]
    return
        if ($ex)
        then
            if ($attributeValue)
            then
                (: If the element must have an attribute, we add the necessary attribute :)
                element {$element} {
                    switch ($element)
                    case "note" return attribute type {$attributeValue}
                    case "scopecontent" return attribute encodinganalog {$attributeValue}
                    default return "",
                    for $value in tokenize($ex, "\|")
                    return
                        <p>{$value}</p>
                }
            else
                (: Otherwise, we generate the appropriate element :)
                element {$element} {
                    if ($element="processinfo")
                    then
                        (: We want the <processinfo> element as a list :)
                        <list>{
                            for $value in tokenize($ex, "\|")
                            return
                                <item>{$value}</item>
                        }</list>
                        
                    else
                        if ($element="bibliography")
                        then
                            (: We want the bibliographic references as <bibref> elements:)
                            for $value in tokenize($ex, "\|")
                            return
                                <bibref>{$value}</bibref>
                        else
                            for $value in tokenize($ex, "\|")
                            return
                                (: Other elements are rendered as <p> :)
                                <p>{$value}</p>
                }
        else
            ()
};

(: This function is used to generate the <controlaccess> element :)
declare function app:controlaccess($item as node()) {
    let $subject := for $value in tokenize($item/child::mcSujet, "\|")
                    return
                        <subject>{$value}</subject>
    let $geogname := for $value in tokenize($item/child::mcLieu, "\|")
                     return
                        <geogname>{$value}</geogname>
    let $persname := for $value in tokenize($item/child::mcPersonne, "\|")
                     return
                        <persname>{$value}</persname>
    let $genreform := for $value in tokenize($item/child::mcGenre, "\|")
                      return
                        <genreform>{$value}</genreform>
    let $controlaccess := ($subject, $geogname, $persname, $genreform)
    return
        (: If there are no keywords, we do not generate an <controlaccess> element :)
        if ($controlaccess)
        then
            <controlaccess>{$controlaccess}</controlaccess>
        else
            ()
};

(: This function is used to translate the level of description into the label used by the EAD :)
declare function app:translateLevel($level as xs:string) {
    switch ($level)
    case "Fonds" return "fonds"
    case "Sous-fonds" return "subfonds"
    case "Collection" return "collection"
    case "Série" return "series"
    case "Sous-série" return "subseries"
    case "Dossier" return "file"
    case "Pièce" return "item"
    case "Record group" return "recordgrp"
    case "subgrp" return "subgrp"
    case "otherlevel" return "otherlevel"
    default return ""
};

(: This function is used to generate all non-child elements of <did> using the app:element, app:arrangement, app:controlaccess, app:accessrestrict and app:userestrict functions :)
declare function app:description($item as node(), $efe as xs:string) {
    let $scopecontent := app:element($item, "porteeContenu", "scopecontent", "3.3.1")
    let $arrangement := app:arrangement($item, $efe)
    let $custodhist := app:element($item, "historiqueGardeDocuments", "custodhist", ())
    let $acqinfo := app:element($item, "acquisition", "acqinfo", ())
    let $appraisal := app:element($item, "evaluationTri", "appraisal", ())
    let $accruals := app:element($item, "accroissements", "accruals", ())
    let $phystech := app:element($item, "caracteristiquesPhysiques", "phystech", ())
    let $originalsloc := app:element($item, "localisationOriginaux", "originalsloc", ())
    let $altformavail := app:element($item, "localisationCopies", "altformavail", ())
    let $relatedmaterial := app:element($item, "uniteAssociee", "relatedmaterial", ())
    let $bibliography := app:element($item, "notePublication", "bibliography", ())
    let $note := app:element($item, "noteGeneral", "note", "generalNote")
    let $processinfo := app:element($item, "historiqueTraitement", "processinfo", ())
    let $controlaccess := app:controlaccess($item)
    let $accessrestrict := if ($item[child::niveauDescription="Fonds" or child::niveauDescription="Collection"])
                           then
                               app:accessrestrict($efe)
                           else
                               ()
    let $userestrict := if ($item[child::niveauDescription="Fonds" or child::niveauDescription="Collection"])
                         then
                            app:userestrict($efe)
                         else
                            ()
    return
        ($scopecontent, $arrangement, $custodhist, $acqinfo, $appraisal, $accruals, $phystech, $originalsloc, $altformavail, $relatedmaterial, $bibliography, $note, $processinfo, $accessrestrict, $userestrict, $controlaccess)
};

(: This function is used to generate the <archDesc> element of the element whose child is the <niveauDescription> element with the value "Fonds" or "Collection" :)
declare function app:archDesc($doc as node(), $efe as xs:string) {
    let $e1 := $doc/child::item[child::niveauDescription="Fonds" or child::niveauDescription="Collection"]
    let $id := $e1/child::identifiant
    (: We translate the level of description :)
    let $level := app:translateLevel($e1/child::niveauDescription)
    return
        <archdesc level="{$level}" encodinganalog="ISAD(G)v2">
            {
                (: We generate the <did> element :)
                app:did($doc, $e1, $efe)
            }
            {
                (: We generate all non-child elements of <did> :)
                app:description($e1, $efe)
            }
            {
                (: We generate the <dsc> element, containing the <c> elements corresponding to the description of each child element of the "Fonds" or "Collection" :)
                if ($doc/child::item[child::niveauDescription!="Fonds" or child::niveauDescription!="Collection"])
                then
                    <dsc type="combined">{
                        for $item in $doc/child::item[child::identifiantParent=$id]
                        return
                            app:c($doc, $item, $efe)
                    }</dsc>
                else
                    ()
            }
        </archdesc>
};

(: This function is used to generate the full EAD with the app:eadHeader and app:archDesc functions :)
declare function app:generateEAD($doc as node(), $efe as xs:string) {
    let $eadheader := app:eadHeader($doc, $efe)
    let $archdesc := app:archDesc($doc, $efe)
    let $ead := <ead>
                    {($eadheader, $archdesc)}
                </ead>
    return
        $ead
};