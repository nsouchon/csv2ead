xquery version "3.1";

(: This module is used to delete CSV and XML files stored on the server at regular intervals :)

let $login := xmldb:login("/db/apps/csv2ead/data/csv", "projectAdmin", "password")
let $login := xmldb:login("/db/apps/csv2ead/data/xml", "projectAdmin", "password")
let $collectionCSV := "/db/apps/csv2ead/data/csv"
let $csvFiles := for $csv in  xmldb:get-child-resources($collectionCSV)
                 return
                     xmldb:remove($collectionCSV, $csv)
let $collectionXML := "/db/apps/csv2ead/data/xml"
let $xmlFiles := for $xml in xmldb:get-child-resources($collectionXML)
                 return
                     xmldb:remove($collectionXML, $xml)
return
    <results>
        <message>Fichiers supprimés</message>
    </results>