# csv2ead

## Présentation

L'application [csv2ead](https://csv2ead.ifao.egnet.net) a été développée au sein du service des archives et collections de l'Institut français d'archéologie orientale. Elle permet de convertir des fichiers CSV respectant un modèle défini en fichiers EAD conformes à la DTD EAD 2002.

## Installation

L’application peut être déployée sur toute installation eXist-db en uploadant le fichier [build/csv2ead-1.0.0.xar](https://gitlab.huma-num.fr/nsouchon/csv2ead/-/blob/main/build/csv2ead-1.0.0.xar?ref_type=heads) depuis le Package Manager accessible sur le Dashboard.

Une fois installée, il est nécessaire de modifier les identifiants utilisés dans les fichiers [job.xql](https://gitlab.huma-num.fr/nsouchon/csv2ead/-/blob/main/job.xql?ref_type=heads), [upload.xq](https://gitlab.huma-num.fr/nsouchon/csv2ead/-/blob/main/modules/upload.xq?ref_type=heads) et [getFiles.xq](https://gitlab.huma-num.fr/nsouchon/csv2ead/-/blob/main/modules/getFiles.xq?ref_type=heads).

## Crédits

* Nicolas Souchon

## Licence

[AGPL-3.0](https://gitlab.huma-num.fr/nsouchon/csv2ead/-/blob/main/LICENSE?ref_type=heads)