/* CSV uploading function with Ajax */
$(function () {
    $("#upload").change(function () {
        var files = $("#upload:file")[0].files;
        $(files).each(function () {
            var file = this;
            var formdata = new FormData();
            formdata.append("files", file);
            $("#resultUpload").show();
            /* We upload each CSV file selected by <input id="upload">  and we display the result of the upload */
            $.ajax({
                url: "modules/upload.xq",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    $("#resultUpload").append("<li>Fichier "+file.name+" téléchargé</li>");
                },
                error: function (result) {
                    $("#resultUpload").append("<li>Le fichier "+file.name+" n'a pas pu être téléchargé</li>");
                }
            });
        });
    });
});

/* CSV converting function with Ajax */
$(function () {
    $("#convertCSV").on("click", function () {
        var files = $("#upload:file")[0].files;
        var filesNb = files.length;
        var i = 0;
        var efe = $("#efe").val();
        /* We create a new ZIP folder to store the generated XML/EAD files */
        var zip = new JSZip();
        /* We display the result of the conversion */
        $("#resultConvert").show();
        $(files).each(function () {
            
            var file = this;
            var name = file.name;
            var xml = name.replace("csv", "xml");
            /* We convert each CSV file uploaded during the session into XML/EAD and display the result of the conversion */
            $.ajax({
                url: "modules/getFiles.xq",
                type: "POST",
                data: { efe:efe, file:name },
                success: function (result) {
                    $("#resultConvert").append("<li>Fichier "+name+" converti</li>");
                    /* See https://github.com/Stuk/jszip/issues/600#issuecomment-528792845 */
                    /* We add the generated XML/EAD file to the zip folder generated above */
                    fetch('./data/xml/' + xml)
                        .then(res => res.arrayBuffer())
                        .then(ab => {
                            zip.file(xml, ab);
                            i++;
                            if (i === filesNb) {
                                /* Once the loop is completed, we display the download button */
                                $("#download").show();
                            }
                        });
                        
                },
                error: function (result) {
                    $("#resultConvert").append("<li>Le fichier "+name+" n'a pas pu être converti</li>");
                }
            });
        });
        /* We allow the download of the generated files as ZIP folder */
        $("#downloadZip").on("click", function () {
            zip.generateAsync({type:"blob"}).then(function (blob) {
                saveAs(blob, "eadFiles.zip");
            }, function (err) {
                $("#downloadZip").text(err);
            });
        });
        /* We redefine the value of the <input id="upload"> to an empty string */
        $("#upload").val("");
        /* We call the convertCSV function to disable the #convertCSV button */
        convertCSV();
    });
});

/* Function to enable or disable the #convertCSV button */
function convertCSV () {
    var efe = $("#efe").val();
    /* If an institution is selected and one or more CSV files have been imported */
    if (efe!="" && $("#upload").val()!== undefined) {
        /* We enable the #convertCSV button */
        $("#convertCSV").prop("disabled", false);
    } else {
        /* Otherwise, we disable the #convertCSV button */
        $("#convertCSV").prop("disabled", true);
    }
}

/* We call the convertCSV function when the page opens, to disable the #convertCSV button */
$(function () {
    convertCSV();
});

/* We call the convertCSV function when the value of <select id="efe"> changes */
$(function () {
    $("#efe").change(function () {
        convertCSV();
    });
});

/* We call the convertCSV function when the value of the <input id="upload"> changes */
$(function () {
    $("#upload").change(function () {
        convertCSV();
    });
});

/* Function to get the current year for copyright */
$(function () {
    var year = new Date().getFullYear();
    $(".currentYear").append(year);
});