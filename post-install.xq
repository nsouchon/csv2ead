xquery version "3.0";

(:~ The post-install runs after the actual install and deploy.
 :
 : @version 1.0.0
 :)

import module namespace xdb="http://exist-db.org/xquery/xmldb";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

(: After installing the application, we need to create the data, data/csv and data/xml folders used to store the uploaded CSV and generated XML files :)

xmldb:create-collection($target, "data"),
sm:chmod(xs:anyURI($target || "/data"), "rwxrwxrwx"),
sm:chown(xs:anyURI($target || "/data"), "csv2ead"),
sm:chgrp(xs:anyURI($target || "/data"), "csv2ead"),

xmldb:create-collection($target || "/data", "csv"),
sm:chmod(xs:anyURI($target || "/data/csv"), "rwxrwxrwx"),
sm:chown(xs:anyURI($target || "/data/csv"), "csv2ead"),
sm:chgrp(xs:anyURI($target || "/data/csv"), "csv2ead"),

xmldb:create-collection($target || "/data", "xml"),
sm:chmod(xs:anyURI($target || "/data/xml"), "rwxrwxrwx"),
sm:chown(xs:anyURI($target || "/data/xml"), "csv2ead"),
sm:chgrp(xs:anyURI($target || "/data/xml"), "csv2ead")