xquery version "3.0";

(: This module is used to upload CSV files and store them in the data/csv folder :)

let $collection := "/db/apps/csv2ead/data/csv"
let $filename := request:get-uploaded-file-name("files")
let $data := request:get-uploaded-file-data("files")
let $login := xmldb:login("/db/apps/csv2ead/data/csv", "projectAdmin", "password")
let $store := xmldb:store($collection, $filename, $data)
return
    <results>
        <message>Fichier {$filename} uploadé</message>
    </results>